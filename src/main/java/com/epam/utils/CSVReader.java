package com.epam.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.nonNull;

public class CSVReader {
    private static final Logger LOG = LogManager.getLogger(CSVReader.class);

    public static Object[][] readFromCSV(String filePath, String delimiter){
        List<String[]> data = new ArrayList<>();
        try(BufferedReader reader = new BufferedReader(new FileReader(filePath))){
            String line = "";
            while (nonNull((line = reader.readLine()))){
                data.add(line.split(delimiter));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            LOG.error("File not fount!");
        } catch (IOException e) {
            e.printStackTrace();
            LOG.error("IOException!");
        }
        return data.toArray(new Object[data.size()][]);
    }

    public static Object[][] readFromCSV(String filePath){
        List<String[]> data = new ArrayList<>();
        try(BufferedReader reader = new BufferedReader(new FileReader(filePath))){
            String line = "";
            while (nonNull((line = reader.readLine()))){
                data.add(new String[]{line});
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            LOG.error("File not fount!");
        } catch (IOException e) {
            e.printStackTrace();
            LOG.error("IOException!");
        }
        return data.toArray(new Object[data.size()][]);
    }
}
