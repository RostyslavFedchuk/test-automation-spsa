package com.epam.model;

public enum  Gender {
    MALE("Male"),
    FEMALE("Female"),
    ANY("Any");

    private String name;

    Gender(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
