package com.epam.model;

public enum Maturity {

    BEGINNER("Beginner"),
    INTERMEDIATE("Intermediate"),
    EXPERT("Expert");

    private String name;

    Maturity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
