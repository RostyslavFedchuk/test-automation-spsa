package com.epam.model;

import lombok.*;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class User {
    private int id;

    private String name;

    private String surname;

    private String password;

    private String email;

    private String phoneNumber;

    private int age;

    private Address address;

    private Set<SportTypeMaturity> sportTypes = new HashSet<>();

    private Gender gender;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", age=" + age +
                ", address=" + address +
                ", sportTypes=" + sportTypes +
                ", gender=" + gender +
                ", hasChildren=" + hasChildren +
                '}';
    }

    private boolean hasChildren;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                age == user.age &&
                hasChildren == user.hasChildren &&
                Objects.equals(name, user.name) &&
                Objects.equals(surname, user.surname) &&
                Objects.equals(password, user.password) &&
                Objects.equals(email, user.email) &&
                Objects.equals(phoneNumber, user.phoneNumber) &&
                gender == user.gender;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
