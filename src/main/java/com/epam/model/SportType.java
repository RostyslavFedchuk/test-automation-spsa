package com.epam.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SportType {
    private int id;

    private String name;

    private Set<SportTypeMaturity> sportTypeMaturities = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SportType casted = (SportType) o;
        return id == casted.id &&
                Objects.equals(name, casted.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
