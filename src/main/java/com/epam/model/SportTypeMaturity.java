package com.epam.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SportTypeMaturity implements Serializable {

    private User user;

    private SportType sportType;

    private Maturity maturity;

}
