package com.epam.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Setter
@Getter
@Builder
@ToString(exclude = {"user"})
public class Address {

    private int id;

    private double latitude;

    private double longitude;

    private User user;
}

