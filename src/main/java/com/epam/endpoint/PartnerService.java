package com.epam.endpoint;

import com.epam.model.Criteria;
import io.qameta.allure.Step;
import io.restassured.response.Response;

public class PartnerService extends Service {

    @Step("Getting all criteria step...")
    public Response getAllCriteria() {
        return client.get("api/v1/users/criteria");
    }

    @Step("Getting all criteria by users id = {0} step...")
    public Response getAllCriteriaByUserId(String id) {
        return client.get("api/v1/users/" + id);
    }

    @Step("Saving new criteria = {1} by users id = {0} step...")
    public Response saveCriteria(String usersId, Criteria criteria) {
        return client
                .body(criteria)
                .post("api/v1/users/" + usersId + "/criteria");
    }

    @Step("Delete criteria by users id = {0} step...")
    public Response deleteCriteria(String usersId) {
        return client.delete("api/v1/users/" + usersId + "/criteria");
    }

    @Step("Delete criteria by users id = {0} step...")
    public Response getPartner(String usersId, Criteria criteria) {
        return client
                .body(criteria)
                .post("api/v1/users/" + usersId + "/partners");
    }

}
