package com.epam.endpoint;

import com.epam.model.User;
import io.qameta.allure.Step;
import io.restassured.response.Response;

public class UserService extends Service {

    @Step("Getting all users step ...")
    public Response getAllUsers() {
        return client.get("/api/v1/users");
    }

    @Step("Adding new user step ...")
    public Response addUser(User user) {
        return client
                .body(user)
                .post("/api/v1/users");
    }

    @Step("Getting user by id = {0} step ...")
    public Response getUserById(String id) {
        return client.get("/api/v1/users/" + id);
    }

    @Step("Editing user by id = {0} to new User = {1} step ...")
    public Response editUserById(String id, User newUser) {
        return client
                .body(newUser)
                .put("/api/v1/users/" + id);
    }

    @Step("Deleting user by id = {0} step ...")
    public Response deleteUserById(String id) {
        return client.delete("/api/v1/users/" + id);
    }
}
