package com.epam.endpoint;

import io.qameta.allure.Step;
import io.restassured.response.Response;

public class AddressService extends Service{

    @Step("Getting all addresses step...")
    public Response getAllAddresses() {
        return client.get("/addresses");
    }

    @Step("Getting address by coordinates lat = {0} and long = {1} step...")
    public Response getAddressByCoordinates(String latitude, String longitude) {
        return client.get("/addresses/coordinates?lat=" + latitude + "&long=" + longitude);
    }

    @Step("Getting address by users id = {0} step...")
    public Response getAddressByUsersId(String id) {
        return client.get("/users/" + id + "/address");
    }
}
