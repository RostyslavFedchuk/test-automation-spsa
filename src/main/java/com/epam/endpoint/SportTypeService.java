package com.epam.endpoint;

import io.qameta.allure.Step;
import io.restassured.response.Response;

public class SportTypeService extends Service {

    @Step("Getting all sport types step ...")
    public Response getAllSportTypes() {
        return client.get("/api/v1/sport");
    }

    @Step("Getting sport type by id = {0} step ...")
    public Response getSportTypeById(String id) {
        return client.get("api/v1/sport/" + id);
    }
}
