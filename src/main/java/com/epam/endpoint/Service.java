package com.epam.endpoint;

import io.restassured.specification.RequestSpecification;

import java.util.ResourceBundle;

import static io.restassured.RestAssured.given;

public class Service {
    RequestSpecification client = given()
            .log()
            .all()
            .baseUri(ResourceBundle.getBundle("config").getString("endpointUrl"));
}
