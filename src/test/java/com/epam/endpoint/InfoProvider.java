package com.epam.endpoint;

import com.epam.utils.CSVReader;
import org.testng.annotations.DataProvider;

public class InfoProvider {
    private String dataPath = "src/test/resources/data";

    @DataProvider(name = "correctUsersId")
    public Object[][] getCorrectUsersId() {
        return CSVReader.readFromCSV(dataPath + "/correctUserId.csv");
    }

    @DataProvider(name = "incorrectUsersId")
    public Object[][] getIncorrectUsersId() {
        return CSVReader.readFromCSV(dataPath + "/incorrectUserId.csv");
    }

    @DataProvider(name = "correctCoordinates")
    public Object[][] getCorrectCoordinates() {
        return CSVReader.readFromCSV(dataPath + "/correctCoordinates.csv", ",");
    }

    @DataProvider(name = "incorrectCoordinates")
    public Object[][] getIncorrectCoordinates() {
        return CSVReader.readFromCSV(dataPath + "/incorrectCoordinates.csv", ",");
    }

}
