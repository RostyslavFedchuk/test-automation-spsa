package com.epam.endpoint;

import com.epam.TestListener;
import com.epam.model.Address;
import org.apache.http.HttpStatus;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.testng.Assert.assertNotEquals;

@Listeners({TestListener.class})
public class AddressServiceTest extends InfoProvider {
    private AddressService addressService = new AddressService();

    @Test
    public void getAllAddressesTest() {
        List<Address> addresses = Arrays.asList(addressService.getAllAddresses()
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Address[].class));
        assertNotEquals(addresses, Collections.EMPTY_LIST);
    }

    @Test(dataProvider = "correctUsersId")
    public void getAddressByCorrectUsersIdTest(String id) {
        Address address = addressService.getAddressByUsersId(id)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(Address.class);
        System.out.println(address);
    }

    @Test(dataProvider = "incorrectUsersId")
    public void getAddressByIncorrectUsersIdTest(String id) {
        addressService.getAddressByUsersId(id)
                .then()
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProvider = "incorrectCoordinates")
    public void getAddressByIncorrectCoordinatesTest(String latitude, String longitude) {
        addressService.getAddressByCoordinates(latitude, longitude)
                .then()
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(dataProvider = "correctCoordinates", enabled = false)
    public void getAddressByCorrectCoordinatesTest(String latitude, String longitude) {
        addressService.getAddressByCoordinates(latitude, longitude)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("user", notNullValue());
    }

}
